package codingChallenge.shortestPath.Model;

/**
 * Diese Klasse repräsentiert eine Kante zwischen zwei Planeten.
 * @author Dominik Martens
 *
 */
public class Edge {

	private long source;
	private long target;
	private double cost;
	
	/**
	 * Konstruktor
	 * @param source Index des Startplaneten.
	 * @param target Index des Zielplaneten.
	 * @param cost Distanz zwischen den Planeten.
	 */
	public Edge(long source, long target, double cost) {
		this.source = source;
		this.target = target;
		this.cost = cost;
	}
	
	/**
	 * Gibt den Index des Startplaneten zurück.
	 * @return Index des Startplaneten.
	 */
	public long getSource() {
		return source;
	}
	
	/**
	 * Gibt den Index des Zielplaneten zurück.
	 * @return Index des Zielplaneten.
	 */
	public long getTarget() {
		return target;
	}
	
	/**
	 * Gibt die Distanz zwischen den beiden Planeten zurück.
	 * @return Distanz zwischen den beiden Planeten.
	 */
	public double getCost() {
		return cost;
	}
	
	@Override
	public String toString() {
		return "source: " + source + ", target: " + target + ", cost: " + cost;
	}
}
