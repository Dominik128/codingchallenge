package codingChallenge.shortestPath.Model;

import java.util.ArrayList;

/**
 * Datenstruktur eines Planeten.
 * @author Dominik Martens
 *
 */
public class Planet implements Comparable<Planet>{

	private String label;
	
	// Hilfsvariablen zur Berechnung im Dijkstra-Algorithmus
	private double distance = Double.POSITIVE_INFINITY;
	private ArrayList<Edge> edges = new ArrayList<>();
	private Planet previousPlanet;
	
	/**
	 * Konstruktor.
	 * @param label Label des Planeten.
	 */
	public Planet(String label) {
		this.label = label;
	}
	
	/**
	 * Gibt das Label des Planeten zurück.
	 * @return Label des Planeten.
	 */
	public String getLabel() {
		return label;
	}
	
	/**
	 * Gibt die aktuelle Distanz zu diesem Planeten zurück.
	 * @return aktuelle Distanz zum Planeten.
	 */
	public double getDistance() {
		return distance;
	}
	
	/**
	 * Setzt die Distanz zu diesem Planeten im Dijkstra Algorithmus.
	 * @param distance neue Distanz zu diesem Planeten.
	 */
	public void setDistance(double distance) {
		this.distance = distance;
	}
	
	/**
	 * Fügt eine Kante zur Liste aller Kanten zu anderen Planeten hinzu.
	 * @param toBeAdded Kante, welche zur Liste hinzugefügt werden soll.
	 */
	public void addEdge(Edge toBeAdded) {
		edges.add(toBeAdded);
	}
	
	/**
	 * Gibt die Liste aller Kanten zu anderen Planeten zurück.
	 * @return ArrayList aller Kanten.
	 */
	public ArrayList<Edge> getEdges() {
		return edges;
	}
	
	/**
	 * Setzt den vorherigen besuchten Planeten.
	 * @param previousPlanet vorherig besuchter Planet.
	 */
	public void setPreviousPlanet(Planet previousPlanet) {
		this.previousPlanet = previousPlanet;
	}
	
	/**
	 * Gibt den vorherigen besuchten Planeten zurück. Wird im Dijkstra-Algorithmus gebraucht.
	 * @return vorherig besuchter Planet.
	 */
	public Planet getPreviousPlanet() {
		return previousPlanet;
	}
	
	@Override
	public String toString() {
		return "label: " + label;
	}

	@Override
	public int compareTo(Planet other) {
		return Double.compare(distance, other.getDistance());
	}
}
