package codingChallenge.shortestPath.Algorithm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.PriorityQueue;
import codingChallenge.shortestPath.Model.Edge;
import codingChallenge.shortestPath.Model.Planet;

/**
 * Dijkstra-Algorithmus zur Berechnung von kürzesten Pfaden zwischen Planeten.
 * @author Dominik Martens
 *
 */
public class Dijkstra {
	
	/**
	 * Einstiegspunkt zur Berechnung der kürzesten Route zum Zielplaneten. Berechnung basiert auf den Dijkstra-Algorithmus.
	 * @param planets Liste aller Planeten.
	 * @param start Startplanet.
	 * @param target Zielplanet.
	 * @return Optimale kürzeste Route vom Start- bis zum Zielplaneten. 
	 */
	public static ArrayList<Planet> calculateShortestPath(ArrayList<Planet> planets, Planet start, Planet target) {
		// kürzesten Pfad berechnen
		dijkstraAlgorithm(planets, start, target);
		
		// Route zusammensetzen 
		ArrayList<Planet> planetRoute = getShortestPath(target);
		
		return planetRoute;
	}
	
	/**
	 * Dijkstra-Algorithmus, berechnet den kürzesten Pfad vom Start- bis zum Zielplaneten.
	 * @param planets Liste aller Planeten.
	 * @param start Startplanet.
	 * @param target Zielplanet.
	 */
	private static void dijkstraAlgorithm(ArrayList<Planet> planets, Planet start, Planet target) {
		// Distanz bis zum Startplaneten auf 0 setzen
		start.setDistance(0);
		
		// Prioritätswarteschlange erstellen und Startplanet hinzufügen
		PriorityQueue<Planet> priorityQueue = new PriorityQueue<>();
		priorityQueue.add(start);
		
		// Solange die Warteschlange noch nicht leer ist
		while (!priorityQueue.isEmpty()) {
			// aktuellen Head holen 
			Planet queueHead = priorityQueue.poll();
			
			// Wenn der Head das Ziel ist -> Algorithmus terminieren
			if (queueHead.getLabel().equals(target.getLabel())) {
				return;
			}
			
			// Alle ausgehenden Kanten des aktuellen Planeten holen
			ArrayList<Edge> queueHeadEdges = queueHead.getEdges();
			
			// Wenn Kanten vorhanden sind 
			if (!queueHeadEdges.isEmpty()) {
				
				for (Edge edge : queueHeadEdges) {
					
					// nächsten Zielplaneten der Kante holen -> Entweder als Target oder als Source
					Planet nextPlanet;
					if (planets.get((int) edge.getSource()).getLabel().equals(queueHead.getLabel())) {
						nextPlanet = planets.get((int) edge.getTarget());
					} else {
						nextPlanet = planets.get((int) edge.getSource());
					}
					
					// Entfernung zu diesem Zielplanten berechenen
					double distanceToEdgeTarget = queueHead.getDistance() + edge.getCost();
					
					// Wenn die berechnete Entfernung die kürzeste zum nächsten Planeten ist
					if (distanceToEdgeTarget < nextPlanet.getDistance()) {
						// Zielplanet aus Warteschlange entfernen
						priorityQueue.remove(nextPlanet);
						// kürzeste Distanz zum Zielplaneten updaten 
						nextPlanet.setDistance(distanceToEdgeTarget);
						// aktueller Warteschlangenhead wird vorheriger Planet vom Zielplaneten der Kante
						nextPlanet.setPreviousPlanet(queueHead);
						// Zielplaneten der Kante zur Warteschlange hinzufügen
						priorityQueue.add(nextPlanet);
					}
				}
			}
		}
	}
	
	/**
	 * Auf Basis des Ergebnisses des Dijkstra-Algorithmus, wird nun die kürzeste Route als Liste zusammengesetzt.
	 * @param target Zielplanet.
	 * @return Optimale Route als Liste von Planeten, welche nacheinander besucht werden.
	 */
	private static ArrayList<Planet> getShortestPath(Planet target) {
		ArrayList<Planet> planetsOnRoute = new ArrayList<>();
		
		// Verkettung der vorherigen Planeten durchgehen und zur Liste hinzufügen
		for (Planet planet = target; planet != null; planet = planet.getPreviousPlanet()) {
			planetsOnRoute.add(planet);
		}
		// Liste umkehren
		Collections.reverse(planetsOnRoute);
		return planetsOnRoute;
	}
}
