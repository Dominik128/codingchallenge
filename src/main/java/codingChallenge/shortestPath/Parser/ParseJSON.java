package codingChallenge.shortestPath.Parser;

import java.io.FileReader;
import java.io.Reader;
import java.util.ArrayList;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import codingChallenge.shortestPath.Model.Edge;
import codingChallenge.shortestPath.Model.Planet;

/**
 * 
 * @author Dominik Martens
 *
 */
public class ParseJSON {
	
	private static JSONParser parser = new JSONParser();
	
	/**
	 * 
	 * @param filePath
	 * @return
	 */
	public static ArrayList<Planet> parseNodes(String filePath) {
		ArrayList<Planet> nodes = new ArrayList<>();
		
		try (Reader reader = new FileReader(filePath)) {
			JSONObject jsonObject = (JSONObject) parser.parse(reader);
			JSONArray jsonNodes = (JSONArray) jsonObject.get("nodes");
			
			for (Object object : jsonNodes) {
				JSONObject node = (JSONObject) object;
				
				String label = (String) node.get("label");
				
				nodes.add(new Planet(label));
			}
			
		} catch (Exception e) {
			System.out.println("parsing nodes from JSON-File failed: " + e.getMessage());
		}
		
		return nodes;
	}
	
	/**
	 * 
	 * @param filePath
	 * @return
	 */
	public static ArrayList<Edge> parseEdges(String filePath) {
		ArrayList<Edge> edges = new ArrayList<>();
		
		try (Reader reader = new FileReader(filePath)) {
			JSONObject jsonObject = (JSONObject) parser.parse(reader);
			JSONArray jsonArray = (JSONArray) jsonObject.get("edges");
			
			for (Object object : jsonArray) {
				JSONObject edge = (JSONObject) object;
				
				long source = (long) edge.get("source");
				long target = (long) edge.get("target");
				double cost = (double) edge.get("cost");
				
				edges.add(new Edge(source, target, cost));
			}
			
		} catch (Exception e) {
			System.out.println("parsing edges from JSON-File failed: " + e.getMessage());
		}
		
		return edges;
	}
}
