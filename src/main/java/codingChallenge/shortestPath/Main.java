package codingChallenge.shortestPath;

import java.util.ArrayList;

import codingChallenge.shortestPath.Algorithm.Dijkstra;
import codingChallenge.shortestPath.Model.Edge;
import codingChallenge.shortestPath.Model.Planet;
import codingChallenge.shortestPath.Parser.ParseJSON;

/**
 * 
 * @author Dominik Martens
 *
 */
public class Main {

	private static final String FILEPATH = "generatedGraph.json";
	
	/**
	 * Mainmethode
	 * @param args
	 */
	public static void main(String[] args) {
		
		// Planeten und Kanten aus dem JSON-File parsen
		ArrayList<Planet> planets = ParseJSON.parseNodes(FILEPATH);
		ArrayList<Edge> edges = ParseJSON.parseEdges(FILEPATH);
		
		// Setzen der Kanten innerhalb der Planeten
		for (Edge edge : edges) {
			planets.get((int) edge.getSource()).addEdge(edge);
			planets.get((int) edge.getTarget()).addEdge(edge);
		}
		
		// Ermitteln des Start- und Zielplanets
		Planet target = null;
		Planet start = null;
		
		for (Planet planet : planets) {
			if (planet.getLabel().equals("b3-r7-r4nd7")) {
				target = planet;
			} else if (planet.getLabel().equals("Erde")) {
				start = planet;
			}
		}
		
		// Berechnen des kürzesten Pfades zum Zielplaneten mithilfe des Dijkstra-Algorithmus
		ArrayList<Planet> planetRoute = Dijkstra.calculateShortestPath(planets, start, target);
		
		// Ausgabe der Ergebnisse auf der Konsole
		System.out.println("Optimale Abfolge der Nodes:" + planetRoute);
		System.out.println("Entfernung bis zum Ziel: " + target.getDistance());
	}
	
}
